# etherpad-skin-colibris-outilslibres

New etherpad look used by the french non-profit [Colibris](https://colibris-lemouvement.org)

Fork from skin __colibris__ with small changes.

## Contributors and copyright
Main author : 
 - Sebastian Castro @seballot  

Contributors : 
 - Jean Philippe @genjin
 - Florian @mrflos  

Home image design:
 - Christophe Dreano

**Work distributed under (Apache License, Version 2.0)[https://www.apache.org/licenses/LICENSE-2.0.html]**  


## Suggested etherpad plugins
None of those are obligatory, but we recommand those plugins to have the best UX:
 - [align](https://npmjs.org/package/ep_align "Plugin details") 
 - [author_neat](https://npmjs.org/package/ep_author_neat "Plugin details") 
 - [comments_page](https://npmjs.org/package/ep_comments_page "Plugin details") 
 - [delete_after_delay](https://npmjs.org/package/ep_delete_after_delay "Plugin details") 
 - [delete_empty_pads](https://npmjs.org/package/ep_delete_empty_pads "Plugin details") 
 - [ep_embedded_hyperlinks2](https://www.npmjs.com/package/ep_embedded_hyperlinks2 "Plugin details") 
 - [font_color](https://npmjs.org/package/ep_font_color "Plugin details") 
 - [headings2](https://npmjs.org/package/ep_headings2 "Plugin details")
 - [pads_stats](https://npmjs.org/package/ep_pads_stats "Plugin details")


## Installation
 - copy the files from this repository to your etherpad `/src/static/skin/outilslibres` folder
```
cd /path/to/etherpad-lite/src/static/skin
git clone https://framagit.org/colibris/etherpad-skin-colibris-outilslibres.git outilslibres
```
 - modify `settings.json` :
 ```
 "skinName": "outilslibres",
 ```
 - restart Etherpad (ex: if you use etherpad as a service `service etherpad-lite restart`)

## Changed made from __colibris__ skin
- Change home wallpaper : `index.css` and `images/fond.jpg`
- Add favicon : `favicon.ico`
